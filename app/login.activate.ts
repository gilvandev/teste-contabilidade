import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http } from '@angular/http';
import { RouterExtensions } from 'nativescript-angular/router';
import 'rxjs/add/operator/map';
require("nativescript-localstorage");

@Injectable()
export class LoginActivate implements CanActivate
{
    
    constructor(private http: Http, private router: RouterExtensions) {}

    private url = 'https://api2.cotafacil.net/admin/login/checa_login';    
    
    canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): boolean {   
      if(!localStorage.getItem('token')){
        this.router.navigate(['login'], { clearHistory: true });
        return false;
      } else {
      
        // this.http.post(this.url, {token: localStorage.getItem('token')})
        // .map(response => response.json())
        // .subscribe(response =>{
        //     if(response.status=='erro') {

        //       localStorage.removeItem('token');
        //       localStorage.removeItem('nome');
        //       localStorage.removeItem('empresa');             
        //       localStorage.removeItem('id_usuario');
              
        //       this.router.navigate(['login'], { clearHistory: true });
        //       return false;
        //     }
        //     else return true;
        // }, ()=>{
        //     alert ('Erro de servidor. Tente novamente...')
        // })

        return true;
      }
    }
    
}

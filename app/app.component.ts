import { Component, OnInit, ViewChild } from "@angular/core";
import * as app from "application";
import { RouterExtensions } from "nativescript-angular/router";

require("nativescript-localstorage");

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

    constructor(private routerExtensions: RouterExtensions) {
        
        // Use the component constructor to inject services.
    }

    ngOnInit(): void {
       
    }
}

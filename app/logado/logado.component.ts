import { Component, OnInit, ViewChild } from "@angular/core";
import * as app from "application";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
require("nativescript-localstorage");

@Component({
    selector: "ns-app",
    templateUrl: "./logado/logado.component.html"
})
export class LogadoComponent implements OnInit {
    private _selectedPage: string;
    private _sideDrawerTransition: DrawerTransitionBase;

    public nome: String;
    public email: String;

    constructor(private routerExtensions: RouterExtensions) {
        this.nome = localStorage.getItem("nome");
        this.email = localStorage.getItem("email");
        // Use the component constructor to inject services.
    }

    ngOnInit(): void {
        this._sideDrawerTransition = new SlideInOnTopTransition();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isPageSelected(pageTitle: string): boolean {
        return pageTitle === this._selectedPage;
    }

    
    sair(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('nome');
        localStorage.removeItem('empresa');             
        localStorage.removeItem('id_usuario');
        
        this.routerExtensions.navigate(['login'], { 
            clearHistory: true,
            transition: {
                name: "fade"
            }
        });
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    onNavItemTap(navItemRoute: string, pagina: string): void {
        this._selectedPage = pagina;
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}

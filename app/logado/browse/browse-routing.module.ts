import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { LoginActivate } from "../../login.activate";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { BrowseComponent } from "./browse.component";

const routes: Routes = [
    { path: "", component: BrowseComponent, canActivate: [LoginActivate] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class BrowseRoutingModule { }

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { LogadoRoutingModule } from "./logado-routing.module";
import { LogadoComponent } from "./logado.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptUISideDrawerModule,
        LogadoRoutingModule
    ],
    declarations: [
        LogadoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LogadoModule { }

import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { LoginActivate } from "../../login.activate";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SearchComponent } from "./search.component";

const routes: Routes = [
    { path: "", component: SearchComponent, canActivate: [LoginActivate] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SearchRoutingModule { }

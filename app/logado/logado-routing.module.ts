import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LogadoComponent } from "./logado.component";

const routes: Routes = [{
    path: '',
    component: LogadoComponent,
    children: [
        { path: "home", loadChildren: "./logado/home/home.module#HomeModule" },
        { path: "browse", loadChildren: "./logado/browse/browse.module#BrowseModule"},
        { path: "search", loadChildren: "./logado/search/search.module#SearchModule"},
        { path: "featured", loadChildren: "./logado/featured/featured.module#FeaturedModule"},
        { path: "settings", loadChildren: "./logado/settings/settings.module#SettingsModule"}
    ]
}];


@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LogadoRoutingModule { }

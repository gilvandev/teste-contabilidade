import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { LoginActivate } from "../../login.activate";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SettingsComponent } from "./settings.component";

const routes: Routes = [
    { path: "", component: SettingsComponent, canActivate: [LoginActivate] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule { }

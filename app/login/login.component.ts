import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SnackBar } from "nativescript-snackbar";

require("nativescript-localstorage");

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit {
    public campos: any;
    private _snackbar: SnackBar;

    constructor(private router: RouterExtensions, ) {       
        
        this._snackbar = new SnackBar();
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.campos = {
            cnpj: "",
            senha: ""
        }

        if (localStorage.getItem("token")) this.router.navigate(["/logado/home"], { clearHistory: true });
        // Init your component properties here.
    }

    public login() {
        console.log("Logando...CNPJ: "+this.campos.cnpj+' | Senha: '+this.campos.senha);
        if(this.campos.cnpj && this.campos.senha) {
           
            if(this.campos.cnpj==1) {       
                localStorage.setItem("token", "sdjkasjdbasd");    
                localStorage.setItem("nome", "Gilvan C. Rodrigues");    
                localStorage.setItem("email", "contato@gilvandev.com.br");     
                localStorage.setItem("empresa", "2");       
                localStorage.setItem("id_usuario", "5");          
                this.router.navigate(["/logado/home"], { clearHistory: true });
            } else {
                this._snackbar.simple("CNPJ e/ou senha errado(s)!", "#000", "yellow").then(args => {
                });
            }
        } else {
            this._snackbar.simple("Preencha todos os campos!", "#FFF", "red").then(args => {                    
            });
        }
    }

}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var nativescript_snackbar_1 = require("nativescript-snackbar");
require("nativescript-localstorage");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(router) {
        this.router = router;
        this._snackbar = new nativescript_snackbar_1.SnackBar();
        // Use the component constructor to inject providers.
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.campos = {
            cnpj: "",
            senha: ""
        };
        if (localStorage.getItem("token"))
            this.router.navigate(["/logado/home"], { clearHistory: true });
        // Init your component properties here.
    };
    LoginComponent.prototype.login = function () {
        console.log("Logando...CNPJ: " + this.campos.cnpj + ' | Senha: ' + this.campos.senha);
        if (this.campos.cnpj && this.campos.senha) {
            if (this.campos.cnpj == 1) {
                localStorage.setItem("token", "sdjkasjdbasd");
                localStorage.setItem("nome", "Gilvan C. Rodrigues");
                localStorage.setItem("email", "contato@gilvandev.com.br");
                localStorage.setItem("empresa", "2");
                localStorage.setItem("id_usuario", "5");
                this.router.navigate(["/logado/home"], { clearHistory: true });
            }
            else {
                this._snackbar.simple("CNPJ e/ou senha errado(s)!", "#000", "yellow").then(function (args) {
                });
            }
        }
        else {
            this._snackbar.simple("Preencha todos os campos!", "#FFF", "red").then(function (args) {
            });
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: "Login",
            moduleId: module.id,
            templateUrl: "./login.component.html"
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHNEQUErRDtBQUMvRCwrREFBaUQ7QUFFakQsT0FBTyxDQUFDLDJCQUEyQixDQUFDLENBQUM7QUFPckM7SUFJSSx3QkFBb0IsTUFBd0I7UUFBeEIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFFeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGdDQUFRLEVBQUUsQ0FBQztRQUNoQyxxREFBcUQ7SUFDekQsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHO1lBQ1YsSUFBSSxFQUFFLEVBQUU7WUFDUixLQUFLLEVBQUUsRUFBRTtTQUNaLENBQUE7UUFFRCxFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQ2xHLHVDQUF1QztJQUMzQyxDQUFDO0lBRU0sOEJBQUssR0FBWjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUMsWUFBWSxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEYsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBRXZDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUM5QyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO2dCQUNwRCxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSwwQkFBMEIsQ0FBQyxDQUFDO2dCQUMxRCxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUNuRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsNEJBQTRCLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUk7Z0JBQy9FLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztRQUNMLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLDJCQUEyQixFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQzNFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztJQUNMLENBQUM7SUF2Q1EsY0FBYztRQUwxQixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE9BQU87WUFDakIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx3QkFBd0I7U0FDeEMsQ0FBQzt5Q0FLOEIseUJBQWdCO09BSm5DLGNBQWMsQ0F5QzFCO0lBQUQscUJBQUM7Q0FBQSxBQXpDRCxJQXlDQztBQXpDWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU25hY2tCYXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXNuYWNrYmFyXCI7XHJcblxyXG5yZXF1aXJlKFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiKTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiTG9naW5cIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2xvZ2luLmNvbXBvbmVudC5odG1sXCJcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHB1YmxpYyBjYW1wb3M6IGFueTtcclxuICAgIHByaXZhdGUgX3NuYWNrYmFyOiBTbmFja0JhcjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyRXh0ZW5zaW9ucywgKSB7ICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuX3NuYWNrYmFyID0gbmV3IFNuYWNrQmFyKCk7XHJcbiAgICAgICAgLy8gVXNlIHRoZSBjb21wb25lbnQgY29uc3RydWN0b3IgdG8gaW5qZWN0IHByb3ZpZGVycy5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhbXBvcyA9IHtcclxuICAgICAgICAgICAgY25wajogXCJcIixcclxuICAgICAgICAgICAgc2VuaGE6IFwiXCJcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpKSB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvbG9nYWRvL2hvbWVcIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgICAgIC8vIEluaXQgeW91ciBjb21wb25lbnQgcHJvcGVydGllcyBoZXJlLlxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBsb2dpbigpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxvZ2FuZG8uLi5DTlBKOiBcIit0aGlzLmNhbXBvcy5jbnBqKycgfCBTZW5oYTogJyt0aGlzLmNhbXBvcy5zZW5oYSk7XHJcbiAgICAgICAgaWYodGhpcy5jYW1wb3MuY25waiAmJiB0aGlzLmNhbXBvcy5zZW5oYSkge1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZih0aGlzLmNhbXBvcy5jbnBqPT0xKSB7ICAgICAgIFxyXG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ0b2tlblwiLCBcInNkamthc2pkYmFzZFwiKTsgICAgXHJcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm5vbWVcIiwgXCJHaWx2YW4gQy4gUm9kcmlndWVzXCIpOyAgICBcclxuICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZW1haWxcIiwgXCJjb250YXRvQGdpbHZhbmRldi5jb20uYnJcIik7ICAgICBcclxuICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZW1wcmVzYVwiLCBcIjJcIik7ICAgICAgIFxyXG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJpZF91c3VhcmlvXCIsIFwiNVwiKTsgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvbG9nYWRvL2hvbWVcIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc25hY2tiYXIuc2ltcGxlKFwiQ05QSiBlL291IHNlbmhhIGVycmFkbyhzKSFcIiwgXCIjMDAwXCIsIFwieWVsbG93XCIpLnRoZW4oYXJncyA9PiB7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NuYWNrYmFyLnNpbXBsZShcIlByZWVuY2hhIHRvZG9zIG9zIGNhbXBvcyFcIiwgXCIjRkZGXCIsIFwicmVkXCIpLnRoZW4oYXJncyA9PiB7ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=
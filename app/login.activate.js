"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("nativescript-angular/router");
require("rxjs/add/operator/map");
require("nativescript-localstorage");
var LoginActivate = /** @class */ (function () {
    function LoginActivate(http, router) {
        this.http = http;
        this.router = router;
        this.url = 'https://api2.cotafacil.net/admin/login/checa_login';
    }
    LoginActivate.prototype.canActivate = function (route, state) {
        if (!localStorage.getItem('token')) {
            this.router.navigate(['login'], { clearHistory: true });
            return false;
        }
        else {
            // this.http.post(this.url, {token: localStorage.getItem('token')})
            // .map(response => response.json())
            // .subscribe(response =>{
            //     if(response.status=='erro') {
            //       localStorage.removeItem('token');
            //       localStorage.removeItem('nome');
            //       localStorage.removeItem('empresa');             
            //       localStorage.removeItem('id_usuario');
            //       this.router.navigate(['login'], { clearHistory: true });
            //       return false;
            //     }
            //     else return true;
            // }, ()=>{
            //     alert ('Erro de servidor. Tente novamente...')
            // })
            return true;
        }
    };
    LoginActivate = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, router_1.RouterExtensions])
    ], LoginActivate);
    return LoginActivate;
}());
exports.LoginActivate = LoginActivate;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uYWN0aXZhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsb2dpbi5hY3RpdmF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUUzQyxzQ0FBcUM7QUFDckMsc0RBQStEO0FBQy9ELGlDQUErQjtBQUMvQixPQUFPLENBQUMsMkJBQTJCLENBQUMsQ0FBQztBQUdyQztJQUdJLHVCQUFvQixJQUFVLEVBQVUsTUFBd0I7UUFBNUMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBRXhELFFBQUcsR0FBRyxvREFBb0QsQ0FBQztJQUZBLENBQUM7SUFJcEUsbUNBQVcsR0FBWCxVQUNFLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLEVBQUUsQ0FBQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDZixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFFTixtRUFBbUU7WUFDbkUsb0NBQW9DO1lBQ3BDLDBCQUEwQjtZQUMxQixvQ0FBb0M7WUFFcEMsMENBQTBDO1lBQzFDLHlDQUF5QztZQUN6Qyx5REFBeUQ7WUFDekQsK0NBQStDO1lBRS9DLGlFQUFpRTtZQUNqRSxzQkFBc0I7WUFDdEIsUUFBUTtZQUNSLHdCQUF3QjtZQUN4QixXQUFXO1lBQ1gscURBQXFEO1lBQ3JELEtBQUs7WUFFTCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsQ0FBQztJQUNILENBQUM7SUFwQ1EsYUFBYTtRQUR6QixpQkFBVSxFQUFFO3lDQUlpQixXQUFJLEVBQWtCLHlCQUFnQjtPQUh2RCxhQUFhLENBc0N6QjtJQUFELG9CQUFDO0NBQUEsQUF0Q0QsSUFzQ0M7QUF0Q1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQ2FuQWN0aXZhdGUsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XHJcbnJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlXCIpO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTG9naW5BY3RpdmF0ZSBpbXBsZW1lbnRzIENhbkFjdGl2YXRlXHJcbntcclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwLCBwcml2YXRlIHJvdXRlcjogUm91dGVyRXh0ZW5zaW9ucykge31cclxuXHJcbiAgICBwcml2YXRlIHVybCA9ICdodHRwczovL2FwaTIuY290YWZhY2lsLm5ldC9hZG1pbi9sb2dpbi9jaGVjYV9sb2dpbic7ICAgIFxyXG4gICAgXHJcbiAgICBjYW5BY3RpdmF0ZShcclxuICAgICAgcm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbiAgICApOiBib29sZWFuIHsgICBcclxuICAgICAgaWYoIWxvY2FsU3RvcmFnZS5nZXRJdGVtKCd0b2tlbicpKXtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2xvZ2luJ10sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgXHJcbiAgICAgICAgLy8gdGhpcy5odHRwLnBvc3QodGhpcy51cmwsIHt0b2tlbjogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJyl9KVxyXG4gICAgICAgIC8vIC5tYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKVxyXG4gICAgICAgIC8vIC5zdWJzY3JpYmUocmVzcG9uc2UgPT57XHJcbiAgICAgICAgLy8gICAgIGlmKHJlc3BvbnNlLnN0YXR1cz09J2Vycm8nKSB7XHJcblxyXG4gICAgICAgIC8vICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCd0b2tlbicpO1xyXG4gICAgICAgIC8vICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdub21lJyk7XHJcbiAgICAgICAgLy8gICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2VtcHJlc2EnKTsgICAgICAgICAgICAgXHJcbiAgICAgICAgLy8gICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2lkX3VzdWFyaW8nKTtcclxuICAgICAgICAgICAgICBcclxuICAgICAgICAvLyAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2xvZ2luJ10sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgICAgIC8vICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICBlbHNlIHJldHVybiB0cnVlO1xyXG4gICAgICAgIC8vIH0sICgpPT57XHJcbiAgICAgICAgLy8gICAgIGFsZXJ0ICgnRXJybyBkZSBzZXJ2aWRvci4gVGVudGUgbm92YW1lbnRlLi4uJylcclxuICAgICAgICAvLyB9KVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbn1cclxuIl19
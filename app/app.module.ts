import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { HttpModule } from '@angular/http';
import { SnackBar } from "nativescript-snackbar";
import { LoginActivate } from './login.activate';
import { AppRoutingModule } from "./app-routing.module";
import { LogadoModule } from "./logado/logado.module";
import { AppComponent } from "./app.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        HttpModule,
        LogadoModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        LoginActivate,
        SnackBar
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
